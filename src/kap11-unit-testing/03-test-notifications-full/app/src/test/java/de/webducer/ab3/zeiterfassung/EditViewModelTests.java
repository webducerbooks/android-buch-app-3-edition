package de.webducer.ab3.zeiterfassung;

import androidx.databinding.Observable;

import org.junit.Test;

import java.util.Calendar;

import de.webducer.ab3.zeiterfassung.viewmodels.EditViewModel;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.*;

public class EditViewModelTests {
  @Test
  public void setPause_withDifferentDurations_raiseChangeEvent() {
    // Arrange
    final int initPause = 10;
    final int newPause = 15;
    EditViewModel sut = new EditViewModel(null, 1);
    Observable.OnPropertyChangedCallback listener = mock(Observable.OnPropertyChangedCallback.class);
    sut.setPause(initPause);
    sut.addOnPropertyChangedCallback(listener);

    // Act
    sut.setPause(newPause);

    // Assert
    verify(listener, // Event
        times(1)) // Wie oft
        .onPropertyChanged(any(Observable.class), // Methode
            eq(BR.pause)); // Eigenschaft
    assertThat(sut.getPause())
        .isEqualTo(newPause);
  }

  @Test
  public void setPause_withSameDurations_raiseNoChangeEvent() {
    // Arrange
    final int initPause = 10;
    final int newPause = 10;
    EditViewModel sut = new EditViewModel(null, 1);
    Observable.OnPropertyChangedCallback listener = mock(Observable.OnPropertyChangedCallback.class);
    sut.setPause(initPause);
    sut.addOnPropertyChangedCallback(listener);

    // Act
    sut.setPause(newPause);

    // Assert
    verify(listener, // Event
        times(0)) // Wie oft
        .onPropertyChanged(any(Observable.class), // Methode
            anyInt()); // Eigenschaft
    assertThat(sut.getPause())
        .isEqualTo(newPause);
  }

  @Test
  public void setComment_withDifferentText_raiseChangeEvent() {
    // Arrange
    final String initComment = "Init";
    final String newComment = "New";
    EditViewModel sut = new EditViewModel(null, 1);
    Observable.OnPropertyChangedCallback listener = mock(Observable.OnPropertyChangedCallback.class);
    sut.setComment(initComment);
    sut.addOnPropertyChangedCallback(listener);

    // Act
    sut.setComment(newComment);

    // Assert
    verify(listener, // Event
        times(1)) // Wie oft
        .onPropertyChanged(any(Observable.class), // Methode
            eq(BR.comment)); // Eigenschaft
    assertThat(sut.getComment())
        .isEqualTo(newComment);
  }

  @Test
  public void setComment_withSameText_raiseNoChangeEvent() {
    // Arrange
    final String initComment = "Init";
    final String newComment = "Init";
    EditViewModel sut = new EditViewModel(null, 1);
    Observable.OnPropertyChangedCallback listener = mock(Observable.OnPropertyChangedCallback.class);
    sut.setComment(initComment);
    sut.addOnPropertyChangedCallback(listener);

    // Act
    sut.setComment(newComment);

    // Assert
    verify(listener, // Event
        times(0)) // Wie oft
        .onPropertyChanged(any(Observable.class), // Methode
            anyInt()); // Eigenschaft
    assertThat(sut.getComment())
        .isEqualTo(newComment);
  }

  @Test
  public void setStartTime_withDifferentTime_raiseChangeEvent() {
    // Arrange
    final Calendar initTime = Calendar.getInstance();
    initTime.set(2020, 10, 22, 18, 45, 0);
    final Calendar newTime = Calendar.getInstance();
    newTime.set(2020, 11, 24, 19, 13, 55);
    EditViewModel sut = new EditViewModel(null, 1);
    Observable.OnPropertyChangedCallback listener = mock(Observable.OnPropertyChangedCallback.class);
    sut.setStartTime(initTime);
    sut.addOnPropertyChangedCallback(listener);

    // Act
    sut.setStartTime(newTime);

    // Assert
    verify(listener, // Event
        times(1)) // Wie oft
        .onPropertyChanged(any(Observable.class), // Methode
            eq(BR.startTime)); // Eigenschaft
    assertThat(sut.getStartTime())
        .isEquivalentAccordingToCompareTo(newTime);
  }

  @Test
  public void setStartTime_withSameTime_raiseChangeEvent() {
    // Arrange
    final Calendar initTime = Calendar.getInstance();
    initTime.set(2020, 10, 22, 18, 45, 0);
    final Calendar newTime = Calendar.getInstance();
    newTime.set(2020, 10, 22, 18, 45, 0);
    EditViewModel sut = new EditViewModel(null, 1);
    Observable.OnPropertyChangedCallback listener = mock(Observable.OnPropertyChangedCallback.class);
    sut.setStartTime(initTime);
    sut.addOnPropertyChangedCallback(listener);

    // Act
    sut.setStartTime(newTime);

    // Assert
    // Event wird gefeuert, da wir bei Start und Endzeit keine Prüfung auf Gleichheit durchführen
    verify(listener, // Event
        times(1)) // Wie oft
        .onPropertyChanged(any(Observable.class), // Methode
            anyInt()); // Eigenschaft
    assertThat(sut.getStartTime())
        .isEqualTo(newTime);
  }

  @Test
  public void setEndTime_withDifferentTime_raiseChangeEvent() {
    // Arrange
    final Calendar initTime = Calendar.getInstance();
    initTime.set(2020, 10, 22, 18, 45, 0);
    final Calendar newTime = Calendar.getInstance();
    newTime.set(2020, 11, 24, 19, 13, 55);
    EditViewModel sut = new EditViewModel(null, 1);
    Observable.OnPropertyChangedCallback listener = mock(Observable.OnPropertyChangedCallback.class);
    sut.setEndTime(initTime);
    sut.addOnPropertyChangedCallback(listener);

    // Act
    sut.setEndTime(newTime);

    // Assert
    verify(listener, // Event
        times(1)) // Wie oft
        .onPropertyChanged(any(Observable.class), // Methode
            eq(BR.endTime)); // Eigenschaft
    assertThat(sut.getEndTime())
        .isEquivalentAccordingToCompareTo(newTime);
  }

  @Test
  public void setEndTime_withSameTime_raiseChangeEvent() {
    // Arrange
    final Calendar initTime = Calendar.getInstance();
    initTime.set(2020, 10, 22, 18, 45, 0);
    final Calendar newTime = Calendar.getInstance();
    newTime.set(2020, 10, 22, 18, 45, 0);
    EditViewModel sut = new EditViewModel(null, 1);
    Observable.OnPropertyChangedCallback listener = mock(Observable.OnPropertyChangedCallback.class);
    sut.setEndTime(initTime);
    sut.addOnPropertyChangedCallback(listener);

    // Act
    sut.setEndTime(newTime);

    // Assert
    // Event wird gefeuert, da wir bei Start und Endzeit keine Prüfung auf Gleichheit durchführen
    verify(listener, // Event
        times(1)) // Wie oft
        .onPropertyChanged(any(Observable.class), // Methode
            anyInt()); // Eigenschaft
    assertThat(sut.getEndTime())
        .isEqualTo(newTime);
  }
}
