package de.webducer.ab3.zeiterfassung;

import android.content.Context;
import android.database.sqlite.SQLiteConstraintException;

import androidx.room.Room;
import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import static com.google.common.truth.Truth.assertThat;

import java.util.Calendar;
import java.util.List;

import de.webducer.ab3.zeiterfassung.db.CalculatedWorkTime;
import de.webducer.ab3.zeiterfassung.db.WorkTime;
import de.webducer.ab3.zeiterfassung.db.WorkTimeDao;
import de.webducer.ab3.zeiterfassung.db.WorkTimeDatabase;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class WorkTimeDatabaseTests {
  private WorkTimeDatabase _db;
  private WorkTimeDao _dao;

  @Rule
  public ExpectedException _expectedExceptionRule = ExpectedException.none();

  @Before
  public void initDb() {
    Context testContext = ApplicationProvider.getApplicationContext();
    _db = Room.inMemoryDatabaseBuilder(testContext, WorkTimeDatabase.class).build();
    _dao = _db.workTimeDato();
  }

  @After
  public void closeDb() {
    _db.close();
  }

  @Test
  public void add_withData_addData() {
    // Arrange
    final Calendar startTime = Calendar.getInstance();
    startTime.set(2020, 10, 22, 8, 45, 0);
    final Calendar endTime = Calendar.getInstance();
    endTime.set(2020, 10, 22, 19, 13, 55);
    final int pause = 15;
    final String comment = "TEST";
    WorkTime workTime = new WorkTime();
    workTime.startTime = startTime;
    workTime.endTime = endTime;
    workTime.setPause(pause);
    workTime.comment = comment;

    // Zischenprüfung auf leere Datenbank
    assertThat(_dao.getAll()).isEmpty();

    // Act
    _dao.add(workTime);

    // Assert
    assertThat(_dao.getAll()).hasSize(1);
  }

  @Test
  public void add_withoutStartTime_fail() {
    // Arrange
    final Calendar startTime = null;
    final Calendar endTime = Calendar.getInstance();
    endTime.set(2020, 10, 22, 19, 13, 55);
    final int pause = 15;
    final String comment = "TEST";
    WorkTime workTime = new WorkTime();
    workTime.startTime = startTime;
    workTime.endTime = endTime;
    workTime.setPause(pause);
    workTime.comment = comment;

    // Act / Assert
    _expectedExceptionRule.expect(SQLiteConstraintException.class);
    _dao.add(workTime);
  }

  @Test
  public void add_withData_readCalculatedData() {
    // Arrange
    final Calendar startTime = Calendar.getInstance();
    startTime.set(2020, 10, 22, 8, 45, 0);
    final Calendar endTime = Calendar.getInstance();
    endTime.set(2020, 10, 22, 19, 13, 55);
    final int pause = 15;
    final String comment = "TEST";
    WorkTime workTime = new WorkTime();
    workTime.startTime = startTime;
    workTime.endTime = endTime;
    workTime.setPause(pause);
    workTime.comment = comment;

    // Act
    _dao.add(workTime);
    List<CalculatedWorkTime> data = _dao.getAllCalculated();

    // Assert
    assertThat(data).hasSize(1);
    CalculatedWorkTime calculatedData = data.get(0);
    assertThat(calculatedData.workTimeInMinutes).isEqualTo(628);
    assertThat(calculatedData.plainWorkTimeInMinutes).isEqualTo(613);
  }
}