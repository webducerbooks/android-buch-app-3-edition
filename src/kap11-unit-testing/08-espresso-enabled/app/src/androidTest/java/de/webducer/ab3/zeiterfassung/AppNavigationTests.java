package de.webducer.ab3.zeiterfassung;


import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.espresso.ViewInteraction;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isEnabled;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withContentDescription;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class AppNavigationTests {

  @Rule
  public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

  @Test
  public void appNavigationTests() {
    // Prüfung auf Anzeige
    ViewInteraction startButton = onView(withId(R.id.StartCommand));
    ViewInteraction endButton = onView(withId(R.id.EndCommand));
    startButton.check(matches(isDisplayed()));
    startButton.check(matches(isEnabled()));
    endButton.check(matches(not(isEnabled())));

    // Click auslösen
    startButton.perform(click());
    startButton.check(matches(not(isEnabled())));
    endButton.check(matches(isEnabled()));

    // Click auslösen (Beenden)
    endButton.perform(click());
    startButton.check(matches(isEnabled()));
    endButton.check(matches(not(isEnabled())));

    // Click auslösen auf Listen-Menüpunkt
    ViewInteraction listMenu = onView(withId(R.id.MenuItemListData));
    listMenu.perform(click());

    // Aktion auf einem Listenelement ausführen
    onView(withId(R.id.DataList))
        .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

    // Prüfen auf lokalisierten Text
    ViewInteraction comment = onView(withText(R.string.LabelComment));
    comment.check(matches(isDisplayed()));

    // Navigation zurück über ActionBar
    ViewInteraction nav = onView(withContentDescription(R.string.abc_action_bar_up_description));
    nav.perform(click());

    pressBack();

    ViewInteraction endButton2 = onView(withId(R.id.EndCommand));
    endButton2.check(matches(isDisplayed()));
  }

  private static Matcher<View> childAtPosition(
      final Matcher<View> parentMatcher, final int position) {

    return new TypeSafeMatcher<View>() {
      @Override
      public void describeTo(Description description) {
        description.appendText("Child at position " + position + " in parent ");
        parentMatcher.describeTo(description);
      }

      @Override
      public boolean matchesSafely(View view) {
        ViewParent parent = view.getParent();
        return parent instanceof ViewGroup && parentMatcher.matches(parent)
            && view.equals(((ViewGroup) parent).getChildAt(position));
      }
    };
  }
}
