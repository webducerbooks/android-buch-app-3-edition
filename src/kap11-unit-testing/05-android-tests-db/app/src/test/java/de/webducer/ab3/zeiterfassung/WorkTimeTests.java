package de.webducer.ab3.zeiterfassung;

import org.junit.Test;

import de.webducer.ab3.zeiterfassung.db.WorkTime;

import static com.google.common.truth.Truth.assertThat;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class WorkTimeTests {
  @Test
  public void setPause_withNegativeValue_setZero() {
    // Arrange
    final int input = -1;
    final int expected = 0;
    WorkTime sut = new WorkTime();

    // Act
    sut.setPause(input);

    // Assert
    assertThat(sut.getPause()).isEqualTo(expected);
  }

  @Test
  public void setPause_withZeroValue_setZero() {
    // Arrange
    final int input = 0;
    final int expected = 0;
    WorkTime sut = new WorkTime();

    // Act
    sut.setPause(input);

    // Assert
    assertThat(sut.getPause()).isEqualTo(expected);
  }

  @Test
  public void setPause_withPositiveValue_setTheValue() {
    // Arrange
    final int input = 1;
    final int expected = 1;
    WorkTime sut = new WorkTime();

    // Act
    sut.setPause(input);

    // Assert
    assertThat(sut.getPause()).isEqualTo(expected);
  }
}