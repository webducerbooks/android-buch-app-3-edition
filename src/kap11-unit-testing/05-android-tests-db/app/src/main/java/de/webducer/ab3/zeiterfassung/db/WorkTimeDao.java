package de.webducer.ab3.zeiterfassung.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface WorkTimeDao {
  @Query("SELECT * FROM time_data ORDER BY start_time DESC")
  List<WorkTime> getAll();

  @Query("SELECT * FROM time_data WHERE _id = :id")
  WorkTime getById(int id);

  @Query("SELECT * FROM time_data " +
      "WHERE IFNULL(end_time, '') = '' " +
      "ORDER BY _id DESC")
  WorkTime getOpened();

  @Query("SELECT _id, start_time, end_time, pause, comment," +
      "\tCASE\n" +
      "\t\tWHEN end_time IS NULL THEN 0\n" +
      "\tELSE\n" +
      "\t\tCAST((strftime('%s', end_time) - strftime('%s', start_time)) / 60 AS INTEGER)\n" +
      "\tEND AS work_time,\n" +
      "\t\tCASE\n" +
      "\t\tWHEN end_time IS NULL THEN 0\n" +
      "\tELSE\n" +
      "\t\tCAST((strftime('%s', end_time) - strftime('%s', start_time) ) / 60 - pause AS INTEGER)\n" +
      "\tEND AS plain_work_time " +
      "FROM time_data " +
      "ORDER BY start_time DESC")
  List<CalculatedWorkTime> getAllCalculated();

  @Insert
  void add(WorkTime workTime);

  @Update
  void update(WorkTime workTime);

  @Delete
  void delete(WorkTime workTime);
}
