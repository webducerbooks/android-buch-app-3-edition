package de.webducer.ab3.zeiterfassung;

import androidx.databinding.Observable;

import org.junit.Test;

import de.webducer.ab3.zeiterfassung.viewmodels.EditViewModel;

import static com.google.common.truth.Truth.assertThat;
import static org.mockito.Mockito.*;

public class EditViewModelTests {
  @Test
  public void setPause_withDifferentDurations_raiseChangeEvent() {
    // Arrange
    final int initPause = 10;
    final int newPause = 15;
    EditViewModel sut = new EditViewModel(null, 1);
    Observable.OnPropertyChangedCallback listener = mock(Observable.OnPropertyChangedCallback.class);
    sut.setPause(initPause);
    sut.addOnPropertyChangedCallback(listener);

    // Act
    sut.setPause(newPause);

    // Assert
    verify(listener, // Event
        times(1)) // Wie oft
        .onPropertyChanged(any(Observable.class), // Methode
            eq(BR.pause)); // Eigenschaft
    assertThat(sut.getPause())
        .isEqualTo(newPause);
  }

  @Test
  public void setPause_withSameDurations_raiseNoChangeEvent() {
    // Arrange
    final int initPause = 10;
    final int newPause = 10;
    EditViewModel sut = new EditViewModel(null, 1);
    Observable.OnPropertyChangedCallback listener = mock(Observable.OnPropertyChangedCallback.class);
    sut.setPause(initPause);
    sut.addOnPropertyChangedCallback(listener);

    // Act
    sut.setPause(newPause);

    // Assert
    verify(listener, // Event
        times(0)) // Wie oft
        .onPropertyChanged(any(Observable.class), // Methode
            anyInt()); // Eigenschaft
    assertThat(sut.getPause())
        .isEqualTo(newPause);
  }
}
