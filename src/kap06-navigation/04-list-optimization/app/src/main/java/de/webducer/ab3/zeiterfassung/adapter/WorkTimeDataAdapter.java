package de.webducer.ab3.zeiterfassung.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.List;

import de.webducer.ab3.zeiterfassung.R;
import de.webducer.ab3.zeiterfassung.db.WorkTime;

public class WorkTimeDataAdapter extends RecyclerView.Adapter<WorkTimeDataAdapter.WorkTimeViewHolder> {
  private Context _context;
  private List<WorkTime> _data;
  private DateFormat _dateFormatter;
  private DateFormat _timeFormatter;

  public WorkTimeDataAdapter(Context context) {
    _context = context;
    // Initializierung Datum / Uhrzeit Formatierung
    _dateFormatter = android.text.format.DateFormat.getDateFormat(_context);
    _timeFormatter = android.text.format.DateFormat.getTimeFormat(_context);
  }

  @NonNull
  @Override
  public WorkTimeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(_context);
    //View view = inflater.inflate(R.layout.item_time_data_linear, parent, false);
    //View view = inflater.inflate(R.layout.item_time_data_grid, parent, false);
    //View view = inflater.inflate(R.layout.item_time_data_constraint, parent, false);
    View view = inflater.inflate(R.layout.item_time_data_relative, parent, false);
    return new WorkTimeViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull WorkTimeViewHolder holder, int position) {
    // Keine Daten vorhanden
    if (_data == null) {
      return;
    }

    // Keine Daten für die angegebene Position
    if (position >= _data.size()) {
      return;
    }

    WorkTime currentData = _data.get(position);
    holder.StartTimeView.setText(formatDateTime(currentData.startTime));
    if (currentData.endTime == null) {
      holder.EndTimeView.setText("---");
    } else {
      holder.EndTimeView.setText(formatDateTime(currentData.endTime));
    }
  }

  private String formatDateTime(Calendar currentTime) {
    return String.format(
        "%s %s", // String für Formatierung
        _dateFormatter.format(currentTime.getTime()), // Datum formatiert
        _timeFormatter.format(currentTime.getTime()) // Zeit formatiert
    );
  }

  @Override
  public int getItemCount() {
    if (_data == null) {
      return 0;
    }

    return _data.size();
  }

  public void swapData(List<WorkTime> data) {
    _data = data;
    notifyDataSetChanged();
  }

  static class WorkTimeViewHolder extends RecyclerView.ViewHolder {

    final TextView StartTimeView;
    final TextView EndTimeView;

    public WorkTimeViewHolder(@NonNull View itemView) {
      super(itemView);

      StartTimeView = itemView.findViewById(R.id.StartTimeValue);
      EndTimeView = itemView.findViewById(R.id.EndTimeValue);
    }
  }
}
