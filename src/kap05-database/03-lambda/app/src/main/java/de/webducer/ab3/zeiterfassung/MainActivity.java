package de.webducer.ab3.zeiterfassung;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.text.DateFormat;
import java.util.Calendar;

import de.webducer.ab3.zeiterfassung.db.WorkTime;

public class MainActivity extends AppCompatActivity {
  // Klassenvariablen
  private EditText _startDateTime;
  private EditText _endDateTime;
  private Button _startCommand;
  private Button _endCommand;
  private DateFormat _dateFormatter;
  private DateFormat _timeFormatter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    // "Suchen" der UI Elemente
    _startDateTime = findViewById(R.id.StartDateTime);
    _endDateTime = findViewById(R.id.EndDateTime);
    _startCommand = findViewById(R.id.StartCommand);
    _endCommand = findViewById(R.id.EndCommand);

    // Initializierung Datum / Uhrzeit Formatierung
    _dateFormatter = android.text.format.DateFormat.getDateFormat(this);
    _timeFormatter = android.text.format.DateFormat.getTimeFormat(this);
  }

  @Override
  protected void onResume() {
    super.onResume();

    // Listener registrieren
    _startCommand.setOnClickListener(v -> {
      // Datumsausgabe in UI
      final String currentTimeString = getCurrentDateTime();
      _startDateTime.setText(currentTimeString);
      // In Datenbank speichern
      getApp().getExecutors().diskIO().execute(() -> {
        WorkTime workTime = new WorkTime();
        workTime.startTime = currentTimeString;
        getApp().getDb().workTimeDato().add(workTime);
      });
    });
    _endCommand.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        // Datumsausgabe in UI
        String currentTimeString = getCurrentDateTime();
        _endDateTime.setText(currentTimeString);
      }
    });
  }

  private String getCurrentDateTime() {
    Calendar currentTime = Calendar.getInstance();
    return String.format(
        "%s %s", // String für Formatierung
        _dateFormatter.format(currentTime.getTime()), // Datum formatiert
        _timeFormatter.format(currentTime.getTime()) // Zeit formatiert
    );
  }

  @Override
  protected void onPause() {
    super.onPause();

    // Listener deregistrieren
    _startCommand.setOnClickListener(null);
    _endCommand.setOnClickListener(null);
  }

  private TimeTrackingApp getApp() {
    return (TimeTrackingApp) getApplication();
  }
}