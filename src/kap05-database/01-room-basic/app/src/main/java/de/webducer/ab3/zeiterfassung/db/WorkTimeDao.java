package de.webducer.ab3.zeiterfassung.db;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface WorkTimeDao {
  @Query("SELECT * FROM time_data")
  List<WorkTime> getAll();

  @Query("SELECT * FROM time_data WHERE _id = :id")
  WorkTime getById(int id);

  @Insert
  void add(WorkTime workTime);
}
