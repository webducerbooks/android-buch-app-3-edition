package de.webducer.ab3.zeiterfassung;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import java.text.DateFormat;
import java.util.Calendar;

import de.webducer.ab3.zeiterfassung.db.WorkTime;

public class MainActivity extends AppCompatActivity {
  // Klassenvariablen
  private EditText _startDateTime;
  private EditText _endDateTime;
  private Button _startCommand;
  private Button _endCommand;
  private DateFormat _dateFormatter;
  private DateFormat _timeFormatter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    // "Suchen" der UI Elemente
    _startDateTime = findViewById(R.id.StartDateTime);
    _endDateTime = findViewById(R.id.EndDateTime);
    _startCommand = findViewById(R.id.StartCommand);
    _endCommand = findViewById(R.id.EndCommand);

    // Initializierung Datum / Uhrzeit Formatierung
    _dateFormatter = android.text.format.DateFormat.getDateFormat(this);
    _timeFormatter = android.text.format.DateFormat.getTimeFormat(this);
  }

  @Override
  protected void onStart() {
    super.onStart();
    initFromDb();
  }

  @Override
  protected void onResume() {
    super.onResume();

    // Listener registrieren
    _startCommand.setOnClickListener(v -> {
      _startCommand.setEnabled(false);

      // Datumsausgabe in UI
      // Startzeit wird hier schon korrekt auf das aktuelle Datum gesetzt
      final WorkTime workTime = new WorkTime();
      final String currentTimeString = getCurrentDateTime(workTime.startTime);
      _startDateTime.setText(currentTimeString);
      // In Datenbank speichern
      getApp().getExecutors().diskIO().execute(() -> {
        getApp().getDb().workTimeDato().add(workTime);
      });

      _endCommand.setEnabled(true);
    });
    _endCommand.setOnClickListener(v -> {
      _endCommand.setEnabled(false);

      final Calendar currentTime = Calendar.getInstance();
      getApp().getExecutors().diskIO().execute(() -> {
        WorkTime startedWorkTime = getApp().getDb().workTimeDato().getOpened();
        if (startedWorkTime == null) {
          // Kein Datensatz mit offenem Ende gefunden
          getApp().getExecutors().mainThread()
              .execute(() -> _endDateTime.setText("KEIN OFFENER DATENSATZ GEFUNDEN!"));
        } else {
          startedWorkTime.endTime = currentTime;
          getApp().getDb().workTimeDato().update(startedWorkTime);
          getApp().getExecutors().mainThread()
              .execute(() -> _endDateTime.setText(getCurrentDateTime(currentTime)));
        }
      });

      _startCommand.setEnabled(true);
    });
  }

  @Override
  protected void onPause() {
    super.onPause();

    // Listener deregistrieren
    _startCommand.setOnClickListener(null);
    _endCommand.setOnClickListener(null);
  }

  private TimeTrackingApp getApp() {
    return (TimeTrackingApp) getApplication();
  }

  private void initFromDb() {
    // Deaktivieren der beiden Buttons
    _startCommand.setEnabled(false);
    _endCommand.setEnabled(false);

    // Laden eines offenen Datensatzes
    getApp().getExecutors().diskIO().execute(() -> {
      WorkTime openWorkTile = getApp().getDb().workTimeDato().getOpened();
      if (openWorkTile == null) {
        // Keine offenen Datensätze
        getApp().getExecutors().mainThread().execute(() -> {
          _startDateTime.setText("");
          _endDateTime.setText("");
          _startCommand.setEnabled(true);
        });
      } else {
        // Offener Datensatz
        getApp().getExecutors().mainThread().execute(() -> {
          _startDateTime.setText(getCurrentDateTime(openWorkTile.startTime));
          _endDateTime.setText("");
          _endCommand.setEnabled(true);
        });
      }
    });
  }

  private String getCurrentDateTime(Calendar currentTime) {
    return String.format(
        "%s %s", // String für Formatierung
        _dateFormatter.format(currentTime.getTime()), // Datum formatiert
        _timeFormatter.format(currentTime.getTime()) // Zeit formatiert
    );
  }
}