package de.webducer.ab3.zeiterfassung.dialogs;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.Calendar;

public class ChangeTimeDialog extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

  private IChangeDateTime _changeListener;

  @NonNull
  @Override
  public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
    if (!(getActivity() instanceof IChangeDateTime)) {
      throw new UnsupportedOperationException("Activity muss 'IChangeDateTime' interface implementieren");
    }

    _changeListener = (IChangeDateTime) getActivity();
    IChangeDateTime.DateType type = IChangeDateTime.DateType.valueOf(getTag());
    Calendar date = _changeListener.getDate(type);

    return new TimePickerDialog(getContext(),
        this, // Callback fürs Setzen
        date.get(Calendar.HOUR_OF_DAY),
        date.get(Calendar.MINUTE),
        // Android Einstellung, ob 24h Format genutzt wird
        android.text.format.DateFormat.is24HourFormat(getContext()));
  }

  @Override
  public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
    IChangeDateTime.DateType type = IChangeDateTime.DateType.valueOf(getTag());
    _changeListener.updateTime(type, hourOfDay, minute);
  }
}
