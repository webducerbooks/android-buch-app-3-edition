package de.webducer.ab3.zeiterfassung;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import de.webducer.ab3.zeiterfassung.databinding.ActivityBindingEditDataBinding;
import de.webducer.ab3.zeiterfassung.viewmodels.EditViewModel;

public class ModernEditActivity extends AppCompatActivity {
  public static final String ID_KEY = "WorkTimeId";
  private EditViewModel _vm = null;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Initialisierung von Binding
    ActivityBindingEditDataBinding binding =
        DataBindingUtil.setContentView(this,
            R.layout.activity_binding_edit_data);

    // View Model initialisieren
    _vm = new EditViewModel();
    binding.setVm(_vm);
  }

  @Override
  protected void onStart() {
    super.onStart();

    // Kommentar setzen
    _vm.setComment("Kommentar aus ViewModel");
  }
}
