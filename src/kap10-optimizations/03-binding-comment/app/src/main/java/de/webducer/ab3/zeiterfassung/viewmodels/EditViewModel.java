package de.webducer.ab3.zeiterfassung.viewmodels;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import de.webducer.ab3.zeiterfassung.BR;

public class EditViewModel extends BaseObservable {
  private String _comment;

  @Bindable
  public String getComment() {
    return _comment;
  }

  public void setComment(String comment) {
    // Prüfung auf Änderungen
    if (_comment == null && comment == null) {
      return;
    }
    if (_comment != null && _comment.equals(comment)) {
      return;
    }

    // Änderung übernehmen
    _comment = comment;
    notifyPropertyChanged(BR.comment);
  }
}
