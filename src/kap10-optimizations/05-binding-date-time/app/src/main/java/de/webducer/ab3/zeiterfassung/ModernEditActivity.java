package de.webducer.ab3.zeiterfassung;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import java.util.Calendar;

import de.webducer.ab3.zeiterfassung.databinding.ActivityBindingEditDataBinding;
import de.webducer.ab3.zeiterfassung.db.WorkTime;
import de.webducer.ab3.zeiterfassung.dialogs.ChangeDateDialog;
import de.webducer.ab3.zeiterfassung.dialogs.ChangeTimeDialog;
import de.webducer.ab3.zeiterfassung.dialogs.IChangeDateTime;
import de.webducer.ab3.zeiterfassung.viewmodels.EditViewModel;

public class ModernEditActivity extends AppCompatActivity implements IChangeDateTime {
  public static final String ID_KEY = "WorkTimeId";
  private static final String _START_DATE_TIME = "Key_StartDateTime";
  private static final String _END_DATE_TIME = "Key_EndDateTime";
  private static final String _PAUSE = "Key_Pause";
  private static final String _COMMENT = "Key_Comment";
  private EditViewModel _vm = null;
  private int _workTimeId = -1;
  private boolean _isRestored = false;
  ActivityBindingEditDataBinding _binding;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Initialisierung von Binding
    _binding =
        DataBindingUtil.setContentView(this,
            R.layout.activity_binding_edit_data);

    // View Model initialisieren
    _vm = new EditViewModel();
    _binding.setVm(_vm);

    // Auslesen der übergebenen ID
    _workTimeId = getIntent().getIntExtra(
        ID_KEY, // Key
        -1); // Standardwert

    // Prüfung für Wiederherstellung
    _isRestored = savedInstanceState != null
        && savedInstanceState.containsKey(_START_DATE_TIME);

    // Deaktivieren der Tastatureingaben
    _binding.StartDateValue.setKeyListener(null);
    _binding.StartTimeValue.setKeyListener(null);
    _binding.EndDateValue.setKeyListener(null);
    _binding.EndTimeValue.setKeyListener(null);
  }

  @Override
  protected void onStart() {
    super.onStart();

    // Keine Daten laden, wenn diese Wiederhergestellt wurden
    if (_isRestored) {
      return;
    }

    getApp().getExecutors().diskIO().execute(() -> {
      WorkTime workTime = getApp().getDb().workTimeDato().getById(_workTimeId);
      _vm.setStartTime(workTime.startTime);
      _vm.setEndTime(workTime.endTime);
      _vm.setPause(workTime.getPause());
      _vm.setComment(workTime.comment);
    });
  }

  @Override
  protected void onResume() {
    super.onResume();
    _binding.StartDateValue.setOnClickListener(v -> openDateDialog(IChangeDateTime.DateType.START, true));
    _binding.StartDateValue.setOnFocusChangeListener((v, hasFocus) -> openDateDialog(IChangeDateTime.DateType.START, hasFocus));

    _binding.StartTimeValue.setOnClickListener(v -> openTimeDialog(IChangeDateTime.DateType.START, true));
    _binding.StartTimeValue.setOnFocusChangeListener((v, hasFocus) -> openTimeDialog(IChangeDateTime.DateType.START, hasFocus));

    _binding.EndDateValue.setOnClickListener(v -> openDateDialog(IChangeDateTime.DateType.END, true));
    _binding.EndDateValue.setOnFocusChangeListener((v, hasFocus) -> openDateDialog(IChangeDateTime.DateType.END, hasFocus));

    _binding.EndTimeValue.setOnClickListener(v -> openTimeDialog(IChangeDateTime.DateType.END, true));
    _binding.EndDateValue.setOnFocusChangeListener((v, hasFocus) -> openTimeDialog(IChangeDateTime.DateType.END, hasFocus));
  }

  @Override
  protected void onPause() {
    super.onPause();
    _binding.StartDateValue.setOnClickListener(null);
    _binding.StartDateValue.setOnFocusChangeListener(null);

    _binding.StartTimeValue.setOnClickListener(null);
    _binding.StartTimeValue.setOnFocusChangeListener(null);

    _binding.EndDateValue.setOnClickListener(null);
    _binding.EndDateValue.setOnFocusChangeListener(null);

    _binding.EndTimeValue.setOnClickListener(null);
    _binding.EndTimeValue.setOnFocusChangeListener(null);
  }

  @Override
  protected void onSaveInstanceState(@NonNull Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putLong(_START_DATE_TIME, _vm.getStartTime().getTimeInMillis());
    if (_vm.getEndTime() != null) {
      outState.putLong(_END_DATE_TIME, _vm.getEndTime().getTimeInMillis());
    }
    outState.putInt(_PAUSE, _vm.getPause());
    String comment = _vm.getComment();
    if (comment != null && !comment.isEmpty()) {
      outState.putString(_COMMENT, comment);
    }
  }

  @Override
  protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
    super.onRestoreInstanceState(savedInstanceState);

    long startMillis = savedInstanceState.getLong(_START_DATE_TIME, 0L);
    if (startMillis > 0) {
      Calendar startTime = Calendar.getInstance();
      startTime.setTimeInMillis(startMillis);
      _vm.setStartTime(startTime);
    }

    long endMillis = savedInstanceState.getLong(_END_DATE_TIME, 0L);
    if (endMillis > 0) {
      Calendar endTime = Calendar.getInstance();
      endTime.setTimeInMillis(endMillis);
      _vm.setEndTime(endTime);
    }
    int pause = savedInstanceState.getInt(_PAUSE, 0);
    _vm.setPause(pause);
    _vm.setComment(savedInstanceState.getString(_COMMENT, ""));
  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    saveWorkTime();
  }

  @Override
  public boolean onOptionsItemSelected(@NonNull MenuItem item) {
    switch (item.getItemId()) {
      case android.R.id.home:
        saveWorkTime();
        // Kein return oder break
        // damit Android den Zurück-Button verarbeiten kann
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  @Override
  public Calendar getDate(DateType dateType) {
    if (dateType == DateType.START) {
      return _vm.getStartTime();
    }

    return _vm.getEndTime() == null
        ? Calendar.getInstance()
        : _vm.getEndTime();
  }

  @Override
  public Calendar getTime(DateType dateType) {
    if (dateType == DateType.START) {
      return _vm.getStartTime();
    }

    return _vm.getEndTime() == null
        ? Calendar.getInstance()
        : _vm.getEndTime();
  }

  @Override
  public void updateDate(DateType dateType, int year, int month, int day) {
    if (dateType == DateType.START) {
      Calendar startTime = _vm.getStartTime();
      startTime.set(year, month, day);
      _vm.setStartTime(startTime);
    } else {
      Calendar endTime = _vm.getEndTime();
      if (endTime == null) {
        endTime = Calendar.getInstance();
      }

      endTime.set(year, month, day);
      _vm.setEndTime(endTime);
    }
  }

  @Override
  public void updateTime(DateType dateType, int hours, int minutes) {
    if (dateType == DateType.START) {
      Calendar startTime = _vm.getStartTime();
      startTime.set(Calendar.HOUR_OF_DAY, hours);
      startTime.set(Calendar.MINUTE, minutes);
      _vm.setStartTime(startTime);
    } else {
      Calendar endTime = _vm.getEndTime();
      if (endTime == null) {
        endTime = Calendar.getInstance();
      }

      endTime.set(Calendar.HOUR_OF_DAY, hours);
      endTime.set(Calendar.MINUTE, minutes);
      _vm.setEndTime(endTime);
    }
  }

  private void saveWorkTime() {
    getApp().getExecutors().diskIO().execute(() -> {
      WorkTime workTime = new WorkTime();
      workTime.id = _workTimeId;
      workTime.startTime = _vm.getStartTime();
      workTime.endTime = _vm.getEndTime();
      workTime.setPause(_vm.getPause());
      workTime.comment = _vm.getComment();
      getApp().getDb().workTimeDato().update(workTime);
    });
  }

  private void openDateDialog(DateType type, boolean isFocused) {
    if (!isFocused) {
      return;
    }
    ChangeDateDialog dialog = new ChangeDateDialog();
    dialog.show(getSupportFragmentManager(), type.toString());
  }

  private void openTimeDialog(DateType type, boolean isFocused) {
    if (!isFocused) {
      return;
    }
    ChangeTimeDialog dialog = new ChangeTimeDialog();
    dialog.show(getSupportFragmentManager(), type.toString());
  }

  private TimeTrackingApp getApp() {
    return (TimeTrackingApp) getApplication();
  }
}
