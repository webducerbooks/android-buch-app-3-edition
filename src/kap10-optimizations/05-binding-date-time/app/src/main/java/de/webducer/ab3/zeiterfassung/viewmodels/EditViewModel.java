package de.webducer.ab3.zeiterfassung.viewmodels;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import java.util.Calendar;

import de.webducer.ab3.zeiterfassung.BR;

public class EditViewModel extends BaseObservable {
  private String _comment;
  private int _pause;
  private Calendar _startTime;
  private Calendar _endTime;

  @Bindable
  public String getComment() {
    return _comment;
  }

  public void setComment(String comment) {
    // Prüfung auf Änderungen
    if (_comment == null && comment == null) {
      return;
    }
    if (_comment != null && _comment.equals(comment)) {
      return;
    }

    // Änderung übernehmen
    _comment = comment;
    notifyPropertyChanged(BR.comment);
  }

  @Bindable
  public int getPause() {
    return _pause;
  }

  public void setPause(int pause) {
    if (_pause == pause) {
      return;
    }

    // Änderung übernehmen
    _pause = pause;
    notifyPropertyChanged(BR.pause);
  }

  @Bindable
  public Calendar getStartTime() {
    return _startTime;
  }

  public void setStartTime(Calendar startTime) {
    _startTime = startTime;
    notifyPropertyChanged(BR.startTime);
  }

  @Bindable
  public Calendar getEndTime() {
    return _endTime;
  }

  public void setEndTime(Calendar endTime) {
    _endTime = endTime;
    notifyPropertyChanged(BR.endTime);
  }
}
