package de.webducer.ab3.zeiterfassung;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import de.webducer.ab3.zeiterfassung.databinding.ActivityBindingEditDataBinding;

public class ModernEditActivity extends AppCompatActivity {
  public static final String ID_KEY = "WorkTimeId";

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    // Initialisierung von Binding
    ActivityBindingEditDataBinding binding =
        DataBindingUtil.setContentView(this,
            R.layout.activity_binding_edit_data);

    // Zugriff auf Views
    binding.StartDateValue.setText("Neuer Wert!");
  }
}
