package de.webducer.ab3.zeiterfassung;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.EditText;

import java.text.DateFormat;

import de.webducer.ab3.zeiterfassung.db.WorkTime;

public class EditDataActivity extends AppCompatActivity {
  public static final String ID_KEY = "WorkTimeId";
  private int _workTimeId = -1;
  private WorkTime _workTime = new WorkTime();
  private EditText _startDateValue;
  private EditText _startTimeValue;
  private EditText _endDateValue;
  private EditText _endTimeValue;
  private DateFormat _dateFormatter;
  private DateFormat _timeFormatter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_edit_data);

    // UI Elemente auslesen
    _startDateValue = findViewById(R.id.StartDateValue);
    _startTimeValue = findViewById(R.id.StartTimeValue);
    _endDateValue = findViewById(R.id.EndDateValue);
    _endTimeValue = findViewById(R.id.EndTimeValue);

    // Initializierung Datum / Uhrzeit Formatierung
    _dateFormatter = android.text.format.DateFormat.getDateFormat(this);
    _timeFormatter = android.text.format.DateFormat.getTimeFormat(this);

    // Auslesen der übergebenen ID
    _workTimeId = getIntent().getIntExtra(
        ID_KEY, // Key
        -1); // Standardwert
  }

  @Override
  protected void onStart() {
    super.onStart();
    getApp().getExecutors().diskIO().execute(() -> {
      _workTime = getApp().getDb().workTimeDato().getById(_workTimeId);
      updateUi();
    });
  }

  private void updateUi() {
    getApp().getExecutors().mainThread().execute(() -> {
      _startDateValue.setText(_dateFormatter.format(_workTime.startTime.getTime()));
      _startTimeValue.setText(_timeFormatter.format(_workTime.startTime.getTime()));
      if (_workTime.endTime == null) {
        _endDateValue.setText("");
        _endTimeValue.setText("");
      } else {
        _endDateValue.setText(_dateFormatter.format(_workTime.endTime.getTime()));
        _endTimeValue.setText(_timeFormatter.format(_workTime.endTime.getTime()));
      }
    });
  }

  private TimeTrackingApp getApp() {
    return (TimeTrackingApp) getApplication();
  }
}