package de.webducer.ab3.zeiterfassung.exports;

import android.os.Environment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import de.webducer.ab3.zeiterfassung.ListDataActivity;
import de.webducer.ab3.zeiterfassung.TimeTrackingApp;
import de.webducer.ab3.zeiterfassung.db.WorkTime;

public class CsvExporter {
  private final ListDataActivity _activity;

  public CsvExporter(ListDataActivity activity) {
    _activity = activity;
  }

  public void executeExport() {
    TimeTrackingApp app = (TimeTrackingApp) _activity.getApplication();
    app.getExecutors().diskIO().execute(() -> {
      // Laden der Daten aus der Datenbank
      List<WorkTime> allData = app.getDb().workTimeDato().getAll();
      DateFormat dateTimeFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm", Locale.GERMANY);

      // Export-Ordner (Dokument-Ordner des Benutzers)
      File docsDirectory = _activity.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
      File exportFile = new File(docsDirectory, "WorkTimesExport.csv");

      BufferedWriter writer = null;
      try {
        writer = new BufferedWriter(new FileWriter(exportFile));

        // Speichern der Spalten als erste Zeile
        StringBuilder line = new StringBuilder();
        line.append("id;start_time;end_time;pause;comment");
        writer.append(line);

        // Speichern der Werte
        for (WorkTime workItem : allData) {
          // Zeile leeren
          line.delete(0, line.length());
          // Neue Zeile
          line.append(workItem.id)
              .append(";")
              .append(dateTimeFormatter.format(workItem.startTime.getTime()))
              .append(";");
          if (workItem.endTime == null) {
            line.append(";");
          } else {
            line.append(dateTimeFormatter.format(workItem.endTime.getTime()))
                .append(";");
          }
          line.append(workItem.getPause())
              .append(";");
          if (workItem.comment != null && !workItem.comment.isEmpty()) {
            line.append('"')
                .append(workItem.comment.replace("\"", "\"\""))
                .append('"');
          }

          // Schreiben der Zeile in die Datei
          writer.newLine();
          writer.append(line);
        }
      } catch (IOException e) {
        // Fehler beim Zugriff auf IO
        e.printStackTrace();
      } finally {
        if (writer == null) {
          return;
        }
        try {
          writer.flush();
          writer.close();
        } catch (IOException e) {
          // Fehler beim Schließen des Writers
          e.printStackTrace();
        }
      }
    });
  }
}
