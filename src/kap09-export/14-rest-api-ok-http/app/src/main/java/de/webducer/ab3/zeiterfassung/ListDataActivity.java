package de.webducer.ab3.zeiterfassung;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import de.webducer.ab3.zeiterfassung.adapter.WorkTimeDataAdapter;
import de.webducer.ab3.zeiterfassung.db.WorkTime;
import de.webducer.ab3.zeiterfassung.exports.CsvExporter;
import de.webducer.ab3.zeiterfassung.services.ExportService;

public class ListDataActivity extends AppCompatActivity {
  private final static int _WRITE_REQUEST_CODE = 100;
  private WorkTimeDataAdapter _workTimeAdapter;
  private ProgressBar _exportProgress;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.acitivity_list_data);

    _exportProgress = findViewById(R.id.ExportProgress);

    // Liste suchen
    RecyclerView list = findViewById(R.id.DataList);
    list.setLayoutManager(new LinearLayoutManager(this));

    // Adapter
    _workTimeAdapter = new WorkTimeDataAdapter(this);
    list.setAdapter(_workTimeAdapter);
  }

  @Override
  protected void onStart() {
    super.onStart();
    getApp().getExecutors().diskIO().execute(() -> {
      List<WorkTime> data = getApp().getDb().workTimeDato().getAll();
      getApp().getExecutors().mainThread().execute(() -> {
        _workTimeAdapter.swapData(data);
      });
    });
  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
    switch (requestCode) {
      // Anfragecode von unserer Anfrage
      case _WRITE_REQUEST_CODE:
        if (grantResults.length == 1
            && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
          // Berechtigung wurde erteilt
          exportAsCsv();
        }
        break;
      default:
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        break;
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_list_data_activity, menu);
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(@NonNull MenuItem item) {
    switch (item.getItemId()) {
      case R.id.MenuItemExport:
        tryCsvExport();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  private void tryCsvExport() {
    // Anfrage für Export
    if (checkCallingOrSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
        == PackageManager.PERMISSION_GRANTED) {
      // Berechtigung liegt vor => Export
      exportAsCsv();
    } else {
      // Berechtigung beim Benutzer erfragen
      requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
          _WRITE_REQUEST_CODE);
    }
  }

  private void exportAsCsv() {
    // Export über Intent-Service
    Intent exportService = new Intent(this, ExportService.class);
    startService(exportService);
  }

  private TimeTrackingApp getApp() {
    return (TimeTrackingApp) getApplication();
  }
}
