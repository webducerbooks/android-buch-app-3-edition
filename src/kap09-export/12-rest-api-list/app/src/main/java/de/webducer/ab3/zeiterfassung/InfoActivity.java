package de.webducer.ab3.zeiterfassung;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import de.webducer.ab3.zeiterfassung.adapter.IssueAdapter;
import de.webducer.ab3.zeiterfassung.models.Issue;
import de.webducer.ab3.zeiterfassung.models.Response;

public class InfoActivity extends AppCompatActivity {

  private RecyclerView _issueList;
  private IssueAdapter _adapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_info);

    // Initialisierung der Elemente
    _issueList = findViewById(R.id.IssueList);
    _adapter = new IssueAdapter(this);
    _issueList.setLayoutManager(new LinearLayoutManager(this));
    _issueList.setAdapter(_adapter);
  }

  @Override
  protected void onStart() {
    super.onStart();

    // Laden der Daten
    loadIssues();
  }

  private void loadIssues() {
    getApp().getExecutors().networkIO().execute(() -> {
      // Basis-URL
      String baseUrl = "https://api.bitbucket.org/2.0/repositories/webducerbooks/androidbook-changes/issues";
      String stateFilter = "(state=\"new\" OR state=\"on hold\" OR state=\"open\")";
      String componentFilter = "component!=\"395592\"";
      // Sonderzeichen kodieren
      String filter = Uri.encode(stateFilter + " AND " + componentFilter);
      try {
        // Finale URL
        URL filterUrl = new URL(baseUrl + "?q=" + filter);

        // Erstellen des Clients
        HttpsURLConnection connection = (HttpsURLConnection) filterUrl.openConnection();

        // Defintion der Timeouts
        connection.setReadTimeout(60000); // 1 Minute
        connection.setConnectTimeout(60000); // 1 Minute

        // Anfrage Header definieren
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Accept", "application/json");

        // Verbinden
        connection.connect();

        // Status der Anfrage prüfen
        int statusCode = connection.getResponseCode();
        if (statusCode != 200) {
          // Fehler bei der Abfrage der Daten
          return;
        }

        // Laden der Daten
        InputStream is = connection.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));

        // Lesen der geladenen Daten
        StringBuilder content = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
          content.append(line);
        }

        // Schließen der Resourcen
        reader.close();
        is.close();
        connection.disconnect();

        // Ausgabe der Daten
        //Log.d("TimeTracking", "loadIssues: " + content.toString());

        // Auslesen der Daten (manuell)
        String[] titlesManual = parseIssueTitlesManuall(content.toString());
        // gSON
        String[] titlesGson = parseWithGson(content.toString());
        Issue[] fullIssues = parseFullWithGson(content.toString());

        // Anzeige als HTML
        // String html = generateHtml(fullIssues);
        // showHtml(html);

        // Anzeige als Liste
        showList(fullIssues);
      } catch (MalformedURLException e) {
        // String konnte nicht in URL umgewandelt werden
        e.printStackTrace();
      } catch (IOException e) {
        // Kein Internetverbindung
        e.printStackTrace();
      }
    });
  }

  private String[] parseIssueTitlesManuall(String json) {
    // Prüfen des Inhaltes
    if (json == null || json.isEmpty()) {
      return new String[0];
    }

    try {
      // JSON verarbeiten
      JSONObject response = new JSONObject(json);

      // Issues Liste auslesen
      JSONArray issueList = response.getJSONArray("values");
      if (issueList.length() == 0) {
        return new String[0];
      }

      // Titel auslesen
      List<String> issueTitleList = new ArrayList<>();
      for (int index = 0; index < issueList.length(); index++) {
        JSONObject issue = issueList.getJSONObject(index);
        String title = issue.getString("title");
        Log.d("ISSUE-TITLE: ", title);
        issueTitleList.add(title);
      }

      return issueTitleList.toArray(new String[0]);
    } catch (JSONException e) {
      // Fehler beim Lesen
      e.printStackTrace();
    }

    return new String[0];
  }

  private String[] parseWithGson(String json) {
    // Prüfen des Inhaltes
    if (json == null || json.isEmpty()) {
      return new String[0];
    }

    // Initialisierung der Bibliothek
    Gson gson = new Gson();

    // Deserialisierung von JSON
    Response response = gson.fromJson(json, Response.class);

    // Titel auslesen
    List<String> issueTitleList = new ArrayList<>();
    for (Issue issue : response.values) {
      Log.d("ISSUE-TITLE: ", issue.title);
      issueTitleList.add(issue.title);
    }

    return issueTitleList.toArray(new String[0]);
  }

  private Issue[] parseFullWithGson(String json) {
    // Prüfen des Inhaltes
    if (json == null || json.isEmpty()) {
      return new Issue[0];
    }

    // Initialisierung der Bibliothek
    Gson gson = new Gson();

    // Deserialisierung von JSON
    Response response = gson.fromJson(json, Response.class);

    return response.values;
  }

  private String generateHtml(Issue[] issues) {
    StringBuilder html = new StringBuilder();
    // Header der HTML-Datei
    html.append("<!DOCTYPE html>")
        .append("<html lang=\"en\">")
        .append("<head>")
        .append("<meta charset=\"utf-8\">")
        .append("<title>Fehlerliste</title>")
        .append("</head>");

    // Inhaltsbereich
    html.append("<body>");

    // Fehler ausgeben
    for (Issue issue : issues) {
      // Überschrift
      html.append("<h1>")
          .append("#")
          .append(issue.id)
          .append(" - ")
          .append(issue.title)
          .append("</h1>");

      // Status
      html.append("<div>")
          .append("Status: ")
          .append(issue.state)
          .append("</div>");

      // Priorität
      html.append("<div>")
          .append("Priorität: ")
          .append(issue.priority)
          .append("</div>");

      // Inhalt
      html.append("<section>")
          .append(issue.content.html)
          .append("</section>");
    }

    // HTML finalisieren
    html.append("</body>")
        .append("</html>");

    return html.toString();
  }

  private void showHtml(String htmlContent) {
    getApp().getExecutors().mainThread().execute(() -> {
//      _webContent.loadDataWithBaseURL(
//          null, // Basis URL
//          htmlContent, // HTML
//          "text/html; charset=utf-8", // Mime-Type (Datentyp)
//          "utf-8", // Kodierung
//          null); // Historie
    });
  }

  private void showList(Issue[] issues) {
    getApp().getExecutors().mainThread().execute(() -> {
      _adapter.swapData(Arrays.asList(issues));
    });
  }

  private TimeTrackingApp getApp() {
    return (TimeTrackingApp) getApplication();
  }
}