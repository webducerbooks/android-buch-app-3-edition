package de.webducer.ab3.zeiterfassung;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class InfoActivity extends AppCompatActivity {

  private WebView _webContent;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_info);

    // Initialisierung der Elemente
    _webContent = findViewById(R.id.WebContent);
  }

  @Override
  protected void onStart() {
    super.onStart();

    // Laden der Daten
    loadIssues();
  }

  private void loadIssues() {
    getApp().getExecutors().networkIO().execute(() -> {
      // Basis-URL
      String baseUrl = "https://api.bitbucket.org/2.0/repositories/webducerbooks/androidbook-changes/issues";
      String stateFilter = "(state=\"new\" OR state=\"on hold\" OR state=\"open\")";
      String componentFilter = "component!=\"395592\"";
      // Sonderzeichen kodieren
      String filter = Uri.encode(stateFilter + " AND " + componentFilter);
      try {
        // Finale URL
        URL filterUrl = new URL(baseUrl + "?q=" + filter);

        // Erstellen des Clients
        HttpsURLConnection connection = (HttpsURLConnection) filterUrl.openConnection();

        // Defintion der Timeouts
        connection.setReadTimeout(60000); // 1 Minute
        connection.setConnectTimeout(60000); // 1 Minute

        // Anfrage Header definieren
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Accept", "application/json");

        // Verbinden
        connection.connect();

        // Status der Anfrage prüfen
        int statusCode = connection.getResponseCode();
        if (statusCode != 200) {
          // Fehler bei der Abfrage der Daten
          return;
        }

        // Laden der Daten
        InputStream is = connection.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));

        // Lesen der geladenen Daten
        StringBuilder content = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
          content.append(line);
        }

        // Schließen der Resourcen
        reader.close();
        is.close();
        connection.disconnect();

        // Ausgabe der Daten
        //Log.d("TimeTracking", "loadIssues: " + content.toString());

        // Auslesen der Daten
        String[] titlesManual = parseIssueTitlesManuall(content.toString());
      } catch (MalformedURLException e) {
        // String konnte nicht in URL umgewandelt werden
        e.printStackTrace();
      } catch (IOException e) {
        // Kein Internetverbindung
        e.printStackTrace();
      }
    });
  }

private String[] parseIssueTitlesManuall(String json){
  // Prüfen des Inhaltes
  if(json == null || json.isEmpty()){
    return new String[0];
  }

  try {
    // JSON verarbeiten
    JSONObject response = new JSONObject(json);

    // Issues Liste auslesen
    JSONArray issueList = response.getJSONArray("values");
    if(issueList.length() == 0){
      return new String[0];
    }

    // Titel auslesen
    List<String> issueTitleList = new ArrayList<>();
    for (int index = 0; index < issueList.length(); index++){
      JSONObject issue = issueList.getJSONObject(index);
      String title = issue.getString("title");
      Log.d("ISSUE-TITLE: ", title);
      issueTitleList.add(title);
    }

    return issueTitleList.toArray(new String[0]);
  } catch (JSONException e) {
    // Fehler beim Lesen
    e.printStackTrace();
  }

  return new String[0];
}

  private TimeTrackingApp getApp() {
    return (TimeTrackingApp) getApplication();
  }
}