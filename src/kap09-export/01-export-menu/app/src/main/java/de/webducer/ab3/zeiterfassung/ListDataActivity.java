package de.webducer.ab3.zeiterfassung;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import de.webducer.ab3.zeiterfassung.adapter.WorkTimeDataAdapter;
import de.webducer.ab3.zeiterfassung.db.WorkTime;

public class ListDataActivity extends AppCompatActivity {
  private WorkTimeDataAdapter _workTimeAdapter;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.acitivity_list_data);

    // Liste suchen
    RecyclerView list = findViewById(R.id.DataList);
    list.setLayoutManager(new LinearLayoutManager(this));
    //list.setLayoutManager(new GridLayoutManager(this, 3));

    // Adapter
    _workTimeAdapter = new WorkTimeDataAdapter(this);
    list.setAdapter(_workTimeAdapter);
  }

  @Override
  protected void onStart() {
    super.onStart();
    getApp().getExecutors().diskIO().execute(() -> {
      List<WorkTime> data = getApp().getDb().workTimeDato().getAll();
      getApp().getExecutors().mainThread().execute(() -> {
        _workTimeAdapter.swapData(data);
      });
    });
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.menu_list_data_activity, menu);
    return super.onCreateOptionsMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(@NonNull MenuItem item) {
    switch (item.getItemId()) {
      case R.id.MenuItemExport:
        tryCsvExport();
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  private void tryCsvExport() {
    // Anfrage für Export
  }

  private TimeTrackingApp getApp() {
    return (TimeTrackingApp) getApplication();
  }
}
