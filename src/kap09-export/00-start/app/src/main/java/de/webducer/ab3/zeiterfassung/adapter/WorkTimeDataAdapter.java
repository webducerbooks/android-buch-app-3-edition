package de.webducer.ab3.zeiterfassung.adapter;

import android.app.AlertDialog;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.List;

import de.webducer.ab3.zeiterfassung.EditDataActivity;
import de.webducer.ab3.zeiterfassung.ListDataActivity;
import de.webducer.ab3.zeiterfassung.R;
import de.webducer.ab3.zeiterfassung.TimeTrackingApp;
import de.webducer.ab3.zeiterfassung.db.WorkTime;

public class WorkTimeDataAdapter extends RecyclerView.Adapter<WorkTimeDataAdapter.WorkTimeViewHolder> {
  private ListDataActivity _context;
  private List<WorkTime> _data;
  private DateFormat _dateFormatter;
  private DateFormat _timeFormatter;

  public WorkTimeDataAdapter(ListDataActivity context) {
    _context = context;
    // Initializierung Datum / Uhrzeit Formatierung
    _dateFormatter = android.text.format.DateFormat.getDateFormat(_context);
    _timeFormatter = android.text.format.DateFormat.getTimeFormat(_context);
  }

  @NonNull
  @Override
  public WorkTimeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(_context);
    View view = inflater.inflate(R.layout.item_time_data_linear, parent, false);
    //View view = inflater.inflate(R.layout.item_time_data_grid, parent, false);
    //View view = inflater.inflate(R.layout.item_time_data_constraint, parent, false);
    //View view = inflater.inflate(R.layout.item_time_data_relative, parent, false);
    return new WorkTimeViewHolder(view);
  }

  @Override
  public void onBindViewHolder(@NonNull WorkTimeViewHolder holder, int position) {
    // Keine Daten vorhanden
    if (_data == null) {
      return;
    }

    // Keine Daten für die angegebene Position
    if (position >= _data.size()) {
      return;
    }

    WorkTime currentData = _data.get(position);
    holder.StartTimeView.setText(formatDateTime(currentData.startTime));
    if (currentData.endTime == null) {
      holder.EndTimeView.setText("---");
    } else {
      holder.EndTimeView.setText(formatDateTime(currentData.endTime));
    }

    // Klick auf More registrieren
    holder.MoreIcon.setOnClickListener(v -> {
      PopupMenu menu = new PopupMenu(_context, holder.MoreIcon);
      // Layout des Menüs
      menu.inflate(R.menu.ctx_menu_data_list);
      // Listener setzen
      menu.setOnMenuItemClickListener(item -> handleContextMenu(item, currentData, position));
      // Anzeigen
      menu.show();
    });

    // Bearbeiten auf Klick
    holder.itemView.setOnClickListener(v -> editWorkTime(currentData));
  }

  private boolean handleContextMenu(MenuItem meuItem, WorkTime data, int position) {
    switch (meuItem.getItemId()) {
      case R.id.MenuItemDelete:
        deleteWorkTime(data, position);
        break;

      case R.id.MenuItemEdit:
        editWorkTime(data);
        break;
    }

    return true;
  }

  private void editWorkTime(WorkTime data) {
    Intent editIntent = new Intent(_context, EditDataActivity.class);
    editIntent.putExtra(EditDataActivity.ID_KEY, data.id);

    _context.startActivity(editIntent);
  }

  private void deleteWorkTime(WorkTime workTime, int position) {
    // Dialog konfigurieren
    AlertDialog confirmDialog = new AlertDialog.Builder(_context)
        .setTitle(R.string.DialogTitleDelete)
        .setMessage(R.string.DialogMessageDelete)
        .setNegativeButton(R.string.ButtonCancel, null)
        .setPositiveButton(R.string.MenuItemDelete, (dialog, which) -> {
          // Datensatz löschen
          TimeTrackingApp app = (TimeTrackingApp) _context.getApplication();
          app.getExecutors().diskIO().execute(() -> {
            // Löschen in Datenbank
            app.getDb().workTimeDato().delete(workTime);
            app.getExecutors().mainThread().execute(() -> {
              // Löschen aus der internen Liste
              _data.remove(workTime);
              // Adapter über Löschung informieren
              notifyItemRemoved(position);
              // Dialog schließen
              dialog.dismiss();
            });
          });
        }).create();
    confirmDialog.show();
  }

  private String formatDateTime(Calendar currentTime) {
    return String.format(
        "%s %s", // String für Formatierung
        _dateFormatter.format(currentTime.getTime()), // Datum formatiert
        _timeFormatter.format(currentTime.getTime()) // Zeit formatiert
    );
  }

  @Override
  public int getItemCount() {
    if (_data == null) {
      return 0;
    }

    return _data.size();
  }

  public void swapData(List<WorkTime> data) {
    _data = data;
    notifyDataSetChanged();
  }

  static class WorkTimeViewHolder extends RecyclerView.ViewHolder {

    final TextView StartTimeView;
    final TextView EndTimeView;
    final TextView MoreIcon;

    public WorkTimeViewHolder(@NonNull View itemView) {
      super(itemView);

      StartTimeView = itemView.findViewById(R.id.StartTimeValue);
      EndTimeView = itemView.findViewById(R.id.EndTimeValue);
      MoreIcon = itemView.findViewById(R.id.ShowMoreCommand);
    }
  }
}
