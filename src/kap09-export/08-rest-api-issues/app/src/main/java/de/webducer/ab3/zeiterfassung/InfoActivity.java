package de.webducer.ab3.zeiterfassung;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class InfoActivity extends AppCompatActivity {

  private WebView _webContent;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_info);

    // Initialisierung der Elemente
    _webContent = findViewById(R.id.WebContent);
  }

  @Override
  protected void onStart() {
    super.onStart();

    // Laden der Daten
    loadIssues();
  }

  private void loadIssues() {
    getApp().getExecutors().networkIO().execute(() -> {
      // Basis-URL
      String baseUrl = "https://api.bitbucket.org/2.0/repositories/webducerbooks/androidbook-changes/issues";
      String stateFilter = "(state=\"new\" OR state=\"on hold\" OR state=\"open\")";
      String componentFilter = "component!=\"395592\"";
      // Sonderzeichen kodieren
      String filter = Uri.encode(stateFilter + " AND " + componentFilter);
      try {
        // Finale URL
        URL filterUrl = new URL(baseUrl + "?q=" + filter);

        // Erstellen des Clients
        HttpsURLConnection connection = (HttpsURLConnection) filterUrl.openConnection();

        // Defintion der Timeouts
        connection.setReadTimeout(60000); // 1 Minute
        connection.setConnectTimeout(60000); // 1 Minute

        // Anfrage Header definieren
        connection.setRequestMethod("GET");
        connection.setRequestProperty("Accept", "application/json");

        // Verbinden
        connection.connect();

        // Status der Anfrage prüfen
        int statusCode = connection.getResponseCode();
        if (statusCode != 200) {
          // Fehler bei der Abfrage der Daten
          return;
        }

        // Laden der Daten
        InputStream is = connection.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));

        // Lesen der geladenen Daten
        StringBuilder content = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
          content.append(line);
        }

        // Schließen der Resourcen
        reader.close();
        is.close();
        connection.disconnect();

        // Ausgabe der Daten
        Log.d("TimeTracking", "loadIssues: " + content.toString());
      } catch (MalformedURLException e) {
        // String konnte nicht in URL umgewandelt werden
        e.printStackTrace();
      } catch (IOException e) {
        // Kein Internetverbindung
        e.printStackTrace();
      }
    });
  }

  private TimeTrackingApp getApp() {
    return (TimeTrackingApp) getApplication();
  }
}