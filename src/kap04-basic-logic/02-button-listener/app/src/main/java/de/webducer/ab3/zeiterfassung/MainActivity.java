package de.webducer.ab3.zeiterfassung;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
  // Klassenvariablen
  private EditText _startDateTime;
  private EditText _endDateTime;
  private Button _startCommand;
  private Button _endCommand;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    _startDateTime = findViewById(R.id.StartDateTime);
    _endDateTime = findViewById(R.id.EndDateTime);
    _startCommand = findViewById(R.id.StartCommand);
    _endCommand = findViewById(R.id.EndCommand);
  }

  @Override
  protected void onResume() {
    super.onResume();

    // Listener registrieren
    _startCommand.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        // Logging
        Log.d("MainActivity", // Tag für Filterung
            "onClick für Start-Button aufgerufen"); // Log-Nachricht

        // Toast
        Toast.makeText(MainActivity.this, // Android Context
            "onClick für Start-Button aufgerufen", // Toast-Nachricht
            Toast.LENGTH_LONG) // Anzeigedauer
            .show(); // Toast anzeigen

        // Datumsausgabe in UI
        Calendar currentTime = Calendar.getInstance();
        _startDateTime.setText(currentTime.getTime().toString());
      }
    });
    _endCommand.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        // Logging
        Log.d("MainActivity", // Tag für Filterung
            "onClick für Ende-Button aufgerufen"); // Log-Nachricht

        // Toast
        Toast.makeText(MainActivity.this, // Android Context
            "onClick für Ende-Button aufgerufen", // Toast-Nachricht
            Toast.LENGTH_LONG) // Anzeigedauer
            .show(); // Toast anzeigen

        // Datumsausgabe in UI
        Calendar currentTime = Calendar.getInstance();
        _endDateTime.setText(currentTime.getTime().toString());
      }
    });
  }

  @Override
  protected void onPause() {
    super.onPause();

    // Listener deregistrieren
    _startCommand.setOnClickListener(null);
    _endCommand.setOnClickListener(null);
  }
}