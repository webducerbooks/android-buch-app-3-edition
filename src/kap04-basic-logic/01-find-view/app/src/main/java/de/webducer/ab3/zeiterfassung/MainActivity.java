package de.webducer.ab3.zeiterfassung;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
  // Klassenvariablen
  private EditText _startDateTime;
  private EditText _endDateTime;
  private Button _startCommand;
  private Button _endCommand;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    _startDateTime = findViewById(R.id.StartDateTime);
    _endDateTime = findViewById(R.id.EndDateTime);
    _startCommand = findViewById(R.id.StartCommand);
    _endCommand = findViewById(R.id.EndCommand);
  }
}