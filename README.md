# Android-Apps programmieren

In diesem Projekt finden Sie den Quellcode zu meinem Buch "Android-Apps programmieren" bei [**mitp**-Verlag](https://wdurl.de/ab-mitp).

- [Quelcode Herunterladen](https://bitbucket.org/webducerbooks/android-buch-app-3-edition/get/master.zip)

## Korrekturen

### 20.08.2023

- Update für Android Studio 2022.3 (Giraffe)
	- Gradle Plugin
	- Compile SDK (33)
	- AppCompat
	- Constrained Layout
	- Room
	- RecyclerView
	- Espresso
	- JUnit
	- Test rules

### 04.02.2022

- Update für Android Studio 2021.1 (Bumblebee)
    - Gradle
    - AppCompat
    - Constrained Layout
    - Room
    - Espresso

### 09.08.2021

- Update für Android Studio 2020.03 (Arctic Fox)
    - Gradle
    - AppCompat
    - Constrained Layout

### 15.05.2021

- Update für Android Studio 4.2.2
  - Gradle
  - AppCompat
  - RecyclerView
  - JUnit
  - Espresso

- Update für Android Studio 4.2.1
    - Gradle
    - Gradle Plugin
    - Room
    - RecyclerView
    - OkHttp
    - JUnit
    - Truth
    - Compile SDK version (30)
    - Target SDK verion (30)
    - Java 1.8 support (Room Abhängigkeit)

### 20.01.2021

- Update für Android Studio 4.1.2
    - Gradle Plugin
    - Room
    - Constraint Layout
    - Truth Testbibliothek

### 18.10.2020

- Update für Android Studio 4.1
    - Gradle Plugin
    - Constraint Layout
    - JUnit